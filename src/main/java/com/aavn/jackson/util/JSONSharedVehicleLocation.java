package com.aavn.jackson.util;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "jackson", url = "http://5d00b00ad021760014b751d0.mockapi.io/api")
public interface JSONSharedVehicleLocation {
    @RequestMapping(method = RequestMethod.GET, value = "/json")
    String getData();
}
