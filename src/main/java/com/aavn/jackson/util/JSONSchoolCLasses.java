package com.aavn.jackson.util;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.MediaType;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient(value = "school", url = "http://5d00b00ad021760014b751d0.mockapi.io/api")
public interface JSONSchoolCLasses {
    @RequestMapping(method = RequestMethod.GET, value = "/school")
    String getSchoolData();
}
