package com.aavn.jackson.controller;

import com.aavn.jackson.model.Classes;
import com.aavn.jackson.model.GenericDeserializer;
import com.aavn.jackson.model.School;
import com.aavn.jackson.model.SharedVehicleLocation;
import com.aavn.jackson.util.JSONSchoolCLasses;
import com.aavn.jackson.util.JSONSharedVehicleLocation;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.Arrays;
import java.util.Map;

@RestController
@RequestMapping("/api")
public class HomeController {
    private String json;

    @Value("#{${mapper_shared_vehicle_location}}")
    @Getter
    private Map<String, String> fieldMapper;

    @Value("#{${mapper_school}}")
    @Getter
    private Map<String, String> fieldMapperSchool;

    private JSONSharedVehicleLocation jsonSharedVehicleLocation;

    private JSONSchoolCLasses jsonSchoolCLasses;

    public HomeController(JSONSharedVehicleLocation jsonSharedVehicleLocation, JSONSchoolCLasses jsonSchoolCLasses) {
        this.jsonSharedVehicleLocation = jsonSharedVehicleLocation;
        this.jsonSchoolCLasses = jsonSchoolCLasses;
    }

    //    Test API
    @GetMapping("/")
    public ResponseEntity getDataAPI() {
        return new ResponseEntity(jsonSharedVehicleLocation.getData(), HttpStatus.OK);
    }

    //    Use GenericDeserializer
    @GetMapping("/jackson")
    public ResponseEntity testJacksonGetAPI() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        json = jsonSharedVehicleLocation.getData();
        module.addDeserializer(SharedVehicleLocation.class, new GenericDeserializer<>(SharedVehicleLocation.class, Arrays.asList(SharedVehicleLocation.class), fieldMapper));
        mapper.registerModule(module);

        SharedVehicleLocation item = mapper.readValue(json, SharedVehicleLocation.class);
        return new ResponseEntity(item, HttpStatus.OK);
    }

    //    Test School API
    @GetMapping("/school-api")
    public ResponseEntity getSchoolDataAPI() {
        return new ResponseEntity(jsonSchoolCLasses.getSchoolData(), HttpStatus.OK);
    }

    //    Use GenericDeserializer with MockAPI
    @GetMapping(value = "/school", produces = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity schoolJacksonAPI() throws Exception {
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        json = jsonSchoolCLasses.getSchoolData();
        module.addDeserializer(School.class, new GenericDeserializer<>(School.class, Arrays.asList(School.class, Classes.class), fieldMapperSchool));
        mapper.registerModule(module);

        School[] itemSchool = mapper.readValue(json, School[].class);
        return new ResponseEntity(itemSchool, HttpStatus.OK);
    }
}
