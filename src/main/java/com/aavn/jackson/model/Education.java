package com.aavn.jackson.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Education {
    private String level;
    private String country;
}
