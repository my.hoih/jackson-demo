package com.aavn.jackson.model;

import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

import java.io.IOException;
import java.lang.reflect.Field;
import java.lang.reflect.ParameterizedType;
import java.lang.reflect.Type;
import java.util.*;

/**
 * Generic deserializer based on configurable field mapping, for e.g:
 *  {
 *      "id"        :   "id",
 *      "user.id"   :   "owner.id",
 *      "user.x"    :   "owner.coordinates.0",
 *      "infoName"  :   "vehicles.@1.numberPlate",
 *      "infoValue" :   "info.[name=model].value",
 *  }
 * @param <T>
 */

public class GenericDeserializer<T> extends StdDeserializer<T> {

    private ObjectMapper objectMapper = new ObjectMapper();
    private Map<String, String> fieldMapper;
    protected Class<T> clazz;
    protected List<Class<?>> subClazz;

//    public GenericDeserializer() {
//        super(Object.class);
//    }

    public GenericDeserializer(Class<T> clazz, List<Class<?>> subClazz, Map<String, String> fieldMapper) throws Exception {
        super(clazz);
        this.clazz = clazz;
        this.subClazz = subClazz;
        this.fieldMapper = fieldMapper;
    }

    @Override
    public T deserialize(JsonParser jp, DeserializationContext ctx) throws IOException {
        try {
            JsonNode node = jp.getCodec().readTree(jp);

            if (!isConditionPassed(node))
                return null;

            T item = clazz.newInstance();
            Class<?> currentClazz = clazz;

            do {
                for (Field field: currentClazz.getDeclaredFields()){
                    if (currentClazz != clazz)
                        currentClazz.cast(item);

                    try {
                        field.setAccessible(true);
                        Class<?> type = field.getType();

                        if (!fieldMapper.containsKey(field.getName()))
                            continue;

                        if (isSingularityType(type)){
                            Object value = getFieldValue(node, field.getName(), type);

                            if (value != null)
                                field.set(item, value);

                        } else if (type.isEnum()){
                            Object value = getFieldValue(node, field.getName(), String.class);

                            if (value != null) {
                                for (Object o : type.getEnumConstants()) {
                                    if (String.valueOf(value).equalsIgnoreCase(o.toString())){

                                        field.set(type, o);
                                        break;
                                    }
                                }
                            }

                        } else if (type.equals(List.class) && field.getGenericType() instanceof ParameterizedType) {
                            Type elementType = ((ParameterizedType) field.getGenericType()).getActualTypeArguments()[0];
                            Object value = getFieldValue(node, field.getName(), (Class<?>) elementType);
                            field.set(item, value);

                        } else if (subClazz.contains(type)){
                            Object subItem = subClazz.stream().filter(c -> type == c).findFirst().get().newInstance();

                            for (Field subField: subItem.getClass().getDeclaredFields()){
                                subField.setAccessible(true);
                                Class<?> subType = subField.getType();
                                String subFieldName = field.getName() + "." + subField.getName();

                                if (!fieldMapper.containsKey(subFieldName))
                                    continue;

                                if (isSingularityType(subType)){
                                    Object value = getFieldValue(node, subFieldName, subType);

                                    if (value != null)
                                        subField.set(subItem, value);

                                } else if (subType.isEnum()){
                                    Object value = getFieldValue(node, subFieldName, String.class);

                                    if (value != null) {
                                        for (Object o : subType.getEnumConstants()) {
                                            if (String.valueOf(value).equalsIgnoreCase(o.toString())){
                                                subField.set(subItem, o);
                                                break;
                                            }
                                        }
                                    }
                                }
                            }

                            field.set(item, subItem);
                        }

                    } catch (IllegalAccessException e) {
                        e.printStackTrace();
                    }
                }
            } while ((currentClazz = currentClazz.getSuperclass()) != null);

            return item;
        } catch (Exception e){
            e.printStackTrace();
            return null;
        }

    }

    private boolean isConditionPassed(JsonNode node){
        if (fieldMapper.containsKey("_conditionKey")){
            Object value = getFieldValue(node, "_conditionKey", Object.class);
            String conditionValue = fieldMapper.get("_conditionValue");

            if ("#existed".equals(conditionValue) && value == null)
                return false;
            else if ("#non_existed".equals(conditionValue) && value != null)
                return false;
        }

        return true;
    }

    private boolean isSingularityType(Class<?> type){
        return type.isPrimitive()
                || type.isAssignableFrom(String.class)
                || type.isAssignableFrom(Integer.class)
                || type.isAssignableFrom(Long.class)
                || type.isAssignableFrom(Double.class);
    }

    private Object getFieldValue(JsonNode node, String fieldName, Class<?> clazz){
        Optional<JsonNode> childNode = getNode(node, fieldName);

        if (!childNode.isPresent())
            return null;

        if (childNode.get().isArray()) {
            List<Object> childValues = new ArrayList<>();

            for (JsonNode n : childNode.get()) {
                childValues.add(objectMapper.convertValue(n, clazz));
            }

            return childValues;
        }

        return objectMapper.convertValue(childNode.get(), clazz);
    }

    private Optional<JsonNode> getNode(JsonNode node, String key) {
        String[] arr = fieldMapper.get(key).split("\\.");
        JsonNode jsonNode = node;

        for (String s : arr) {
            if ("@1".equals(s)){
                jsonNode = jsonNode.elements().next();
            } else if (s.startsWith("@@@")){
                ObjectNode fakeNode = JsonNodeFactory.instance.objectNode();
                fakeNode.put("const", s.substring(3, s.length()));

                return Optional.ofNullable(fakeNode.get("const"));
            } else if (jsonNode.isArray()){
                if (s.matches("-?\\d+")){
                    jsonNode = jsonNode.get(Integer.parseInt(s));
                } else if (s.startsWith("[") && s.endsWith("]")){
                    String[] sKey = s.substring(1, s.length()-1).split("=");

                    for (JsonNode sNode: jsonNode){
                        if (sKey[1].equals(sNode.get(sKey[0]).textValue())) {
                            jsonNode = sNode;
                            break;
                        }
                    }
                }

            } else {
                jsonNode = jsonNode.get(s);
            }

            if (jsonNode == null)
                break;
        }

        return Optional.ofNullable(jsonNode);
    }
}