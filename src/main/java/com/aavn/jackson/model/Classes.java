package com.aavn.jackson.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Classes {
    private Long classId;
    private String className;
    private String category;
    private String name;
    public String model;
    public String classNumber;

    public Classes() {}

    public Classes(Long id, String className, String category, String name, String model, String classNumber) {
        classId = id;
        this.className = className;
        this.category = category;
        this.name = name;
        this.model = model;
        this.classNumber = classNumber;
    }
}
