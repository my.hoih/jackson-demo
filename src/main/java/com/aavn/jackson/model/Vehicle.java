package com.aavn.jackson.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class Vehicle {
    private int sourceId;
    private String sourceName;
    private String externalId;
    private String category;
    private String name;
    private String model;
    private String plateNumber;

    public Vehicle() {}

    public Vehicle(int sourceId, String sourceName, String externalId, String category, String name, String model, String plateNumber) {
        this.sourceId = sourceId;
        this.sourceName = sourceName;
        this.externalId = externalId;
        this.category = category;
        this.name = name;
        this.model = model;
        this.plateNumber = plateNumber;
    }
}
