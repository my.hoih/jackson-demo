package com.aavn.jackson.model;

import com.aavn.jackson.model.Classes;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class School extends Education{
    private Double lat;
    private Double lon;
    private Long schoolId;
    private String schoolName;
    private Long departmentSchoolId;
    private String departmentSchoolName;
    private Classes classes;

    public School() {}

    public School(Double lat, Double lon, Long schoolId, String schoolName, Long departmentSchoolId, String departmentSchoolName, Classes classes) {
        this.lat = lat;
        this.lon = lon;
        this.schoolId = schoolId;
        this.schoolName = schoolName;
        this.departmentSchoolId = departmentSchoolId;
        this.departmentSchoolName = departmentSchoolName;
        this.classes = classes;
    }
}
