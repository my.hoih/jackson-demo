package com.aavn.jackson.model;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SharedVehicleLocation {
    private int lat;
    private int lon;
    private int sourceId;
    private String sourceName;
    private String externalId;
    private int mobilityServiceSourceId;
    private String mobilityServiceSourceName;
    private Vehicle vehicle;

    public SharedVehicleLocation() {}

    public SharedVehicleLocation(int lat, int lon, int sourceId, String sourceName, String externalId, int mobilityServiceSourceId, String mobilityServiceSourceName, Vehicle vehicle) {
        this.lat = lat;
        this.lon = lon;
        this.sourceId = sourceId;
        this.sourceName = sourceName;
        this.externalId = externalId;
        this.mobilityServiceSourceId = mobilityServiceSourceId;
        this.mobilityServiceSourceName = mobilityServiceSourceName;
        this.vehicle = vehicle;
    }
}
